package ut.com.nice.jira.plugins.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.nice.jira.plugins.jira.webwork.CreateMergeItemsWebworkModuleAction;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class CreateMergeItemsWebworkModuleActionTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //CreateMergeItemsWebworkModuleAction testClass = new CreateMergeItemsWebworkModuleAction();

        throw new Exception("CreateMergeItemsWebworkModuleAction has no tests!");

    }

}
