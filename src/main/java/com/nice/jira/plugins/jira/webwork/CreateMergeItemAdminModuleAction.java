package com.nice.jira.plugins.jira.webwork;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.html.HTMLDivElement;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.IssueTypeService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.customfields.OperationContextImpl;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

@Named("CreateMergeItemAdminModuleAction")
public class CreateMergeItemAdminModuleAction extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(CreateMergeItemAdminModuleAction.class);

    @ComponentImport
    private final JiraAuthenticationContext authenticationContext;
    
    @ComponentImport
    private final PageBuilderService pageBuilderService;
    
    @ComponentImport
    private final IssueLinkTypeManager issueLinkTypeManager;
    
    @ComponentImport
    private final IssueTypeService issueTypeService;
    
    private Collection<IssueType> issueTypes;
    
    private Collection<IssueLinkType> issueLinkTypes;
    
    private PluginSettings pluginSettings;
    
    private String issuetype = "";
    private String linktype = "";
    
    private static final String ISSUETYPECONFIGKEY = "merge-item.type";
    private static final String LINKTYPECONFIGKEY = "merge-item.link";
    
    @Inject
    public CreateMergeItemAdminModuleAction(
    		JiraAuthenticationContext authenticationContext,
    		PageBuilderService pageBuilderService, 
    		IssueTypeService issueTypeService,
    		IssueLinkTypeManager issueLinkTypeManager)
    {
        this.pageBuilderService = pageBuilderService;
        this.issueTypeService = issueTypeService;
        this.authenticationContext = authenticationContext;
        this.issueLinkTypeManager = issueLinkTypeManager;
        this.pluginSettings = ComponentAccessor.getOSGiComponentInstanceOfType(PluginSettingsFactory.class).createGlobalSettings();
    }
    
    /**
     * The initialization logic of the form.
     * Validation does NOT happen before this.
     *
     * @return the view to display - should usually be "input"
     */
    public String doDefault() throws Exception
    {
        includeResources();
        
        issuetype = pluginSettings.get(ISSUETYPECONFIGKEY) != null ? pluginSettings.get(ISSUETYPECONFIGKEY).toString() : "";
        linktype = pluginSettings.get(LINKTYPECONFIGKEY) != null ? pluginSettings.get(LINKTYPECONFIGKEY).toString() : "";
        
        // Initialization logic
        return INPUT;
    }
    
    /**
     *  The validation logic of you action.  Gets called before doExecute().
     *  Use addError() to add form errors for specific fields.
     *  Use addErrorMessage() for adding generic form errors
     */
    protected void doValidation()
    {
    	HttpServletRequest request = getHttpRequest();
    	
    	issuetype = request.getParameter("issuetype");
    	if(issuetype == null) {
    		addError("issuetype", "Issue type is required.", Reason.VALIDATION_FAILED);
    	}
    	
    	linktype = request.getParameter("linktype");
    	if(linktype == null) {
    		addError("linktype", "Link type is required.", Reason.VALIDATION_FAILED);
    	}
    }
    
    /**
     * The business logic of your form.
     * Only gets called if validation passes.
     *
     * @return the view to display - should usually be "success"
     */
    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
    	pluginSettings.put(ISSUETYPECONFIGKEY, issuetype);
    	pluginSettings.put(LINKTYPECONFIGKEY, linktype);
    	
		return INPUT;
    }
    
    /**
     * Gets issue types for the velocity template.
     * @return a collection of issue types.
     */
    public Collection<IssueType> getIssueTypes(){
    	if(issueTypes == null) {
    		issueTypes = new ArrayList<IssueType>();
    		
    		// Get all standard issue types.
    		for(IssueType it : issueTypeService.getIssueTypes(authenticationContext.getLoggedInUser())){
    			if(!it.isSubTask()) {
    				issueTypes.add(it);
    			}
    		}
    	}
    	
    	return issueTypes;
    }
    
    public Collection<IssueLinkType> getIssueLinkTypes(){
    	if(issueLinkTypes == null) {
    		issueLinkTypes = issueLinkTypeManager.getIssueLinkTypes();
    	}
    	
    	return issueLinkTypes;
    }
    
    private void includeResources() {
        pageBuilderService.assembler().resources().requireWebResource("com.atlassian.auiplugin:aui-select");
    }
}
