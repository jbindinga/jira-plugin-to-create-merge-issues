package com.nice.jira.plugins.jira.webwork;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.CreateValidationResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.customfields.OperationContextImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import webwork.action.ActionContext;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.bc.project.version.VersionService.VersionResult;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.CustomFieldManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

@Named("CreateMergeItemsWebworkModuleAction")
public class CreateMergeItemsWebworkModuleAction extends JiraWebActionSupport
{
    private static final long serialVersionUID = 4038453475811586111L;

	private static final Logger logger = LoggerFactory.getLogger(CreateMergeItemsWebworkModuleAction.class);

    @ComponentImport
    private final IssueService issueService;
    @ComponentImport
    private final JiraAuthenticationContext authenticationContext;
    @ComponentImport
    private final PageBuilderService pageBuilderService;
    @ComponentImport
    private final VersionService versionService;
    @ComponentImport
    private final IssueLinkManager issueLinkManager;
    @ComponentImport
    private final CustomFieldManager customFieldManager;
    
    private Long[] fixVersions;
    private Long id;
    private Collection<CreateValidationResult> createNewValidationResults;
    private HashMap fieldValuesHolder = null;

    @Inject
    public CreateMergeItemsWebworkModuleAction(
    		IssueService issueService, 
    		JiraAuthenticationContext authenticationContext, 
    		PageBuilderService pageBuilderService, 
    		VersionService versionService,
            IssueLinkManager issueLinkManager,
            CustomFieldManager customFieldManager)
    {
        this.issueService = issueService;
        this.authenticationContext = authenticationContext;
        this.pageBuilderService = pageBuilderService;
        this.versionService = versionService;
        this.issueLinkManager = issueLinkManager;
        this.customFieldManager = customFieldManager;
    }

    /**
     *  The validation logic of you action.  Gets called before doExecute().
     *  Use addError() to add form errors for specific fields.
     *  Use addErrorMessage() for adding generic form errors
     */
    protected void doValidation()
    {
    	createNewValidationResults = new ArrayList<CreateValidationResult>();
        includeResources();

        // Populate the valueHolder from the passed in params (used by the form if there is errors)
        getFixVersionField().populateFromParams(getFieldValuesHolder(), ActionContext.getParameters());

        // Get the original issue.
        final Issue issue = getIssueObject();
        
        // Get all versions in the project.
        Project project = issue.getProjectObject();

        List<CustomField> customFieldsForIssue = customFieldManager.getCustomFieldObjects(issue);
        
        // Get the component ids from components set on the issue.
        ArrayList<Long> componentIds = new ArrayList<Long>();
        for(ProjectComponent component : issue.getComponents()){
                componentIds.add(component.getId());
        }
        
        // Validate creating a new issue for every fix version selected.
        for (Long versionId : fixVersions) {
        	// Get fixVersion details
            VersionResult versionResult = versionService.getVersionById(authenticationContext.getLoggedInUser(), versionId);
            if(!versionResult.isValid()){
                this.addErrorCollection(versionResult.getErrorCollection());
                logger.error("The fixVersion with id {0} does not exist.", versionId);
            }

        	String versionName = versionResult.getVersion().getName();
            
            // Create issue for every fixVersion selected.
            final IssueInputParameters issueInputParametersNewIssue = issueService.newIssueInputParameters();
            issueInputParametersNewIssue.setProjectId(project.getId());
            issueInputParametersNewIssue.setIssueTypeId("10000");
            issueInputParametersNewIssue.setReporterId(authenticationContext.getLoggedInUser().getUsername());
            issueInputParametersNewIssue.setFixVersionIds(versionId);
            issueInputParametersNewIssue.setComponentIds(componentIds.toArray(new Long[0]));

            // Copy over any custom fields.
            for(CustomField cf : customFieldsForIssue){
                issueInputParametersNewIssue.addCustomFieldValue(cf.getIdAsLong(), cf.getValueFromIssue(issue));
            }

            issueInputParametersNewIssue.setSummary(versionName + " Merge: " + issue.getSummary());
            issueInputParametersNewIssue.setDescription("Merge to " + versionName + " for " + issue.getKey() + ": " + issue.getSummary() +  "\r\n" + 
            		"{quote}" + issue.getDescription() +"{quote}");
            
            // Validate creation of the issue.
            CreateValidationResult createNewValidation = issueService.validateCreate(authenticationContext.getLoggedInUser(), issueInputParametersNewIssue);
            if (!createNewValidation.isValid())
            {
                this.addErrorCollection(createNewValidation.getErrorCollection());
            }

            // Store validation results so we can use them to execute the creation later.
            createNewValidationResults.add(createNewValidation);
        }
    }

    /**
     * The business logic of your form.
     * Only gets called if validation passes.
     *
     * @return the view to display - should usually be "success"
     */
    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
    	// Create all the issues.
    	for(CreateValidationResult createValidationResult : createNewValidationResults) {
    		final IssueService.IssueResult create = issueService.create(authenticationContext.getLoggedInUser(), createValidationResult);
            if (!create.isValid())
            {
                return ERROR;
            }
            
            // Link the new issue to the source issue.
            issueLinkManager.createIssueLink(getIssue().getId(), create.getIssue().getId(), 10000L, 1L, authenticationContext.getLoggedInUser());
    	}
        
        // We want to redirect back to the view issue page of the original issue.
        return returnComplete("/browse/" + getIssue().getKey());
    }

    /**
     * The initialization logic of the form.
     * Validation does NOT happen before this.
     *
     * @return the view to display - should usually be "input"
     */
    public String doDefault() throws Exception
    {
        final Issue issue = getIssueObject();
        if (issue == null)
        {
            return INPUT;
        }

        includeResources();

        // Check to see if we can create new issues.
        final IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
        issueInputParameters.setProjectId(issue.getProjectObject().getId());
        issueInputParameters.setIssueTypeId("10000");
        issueInputParameters.setReporterId(authenticationContext.getLoggedInUser().getUsername());
        
        // This should validate whether the user is able to create new issues.
        IssueService.CreateValidationResult localResult = issueService.validateCreate(authenticationContext.getLoggedInUser(), issueInputParameters);
        if (!localResult.isValid())
        {
            this.addErrorCollection(localResult.getErrorCollection());
        }

        // Initialization logic
        return INPUT;
    }

    /**
     * Used by the action to render the fixVersions field
     *
     * @return the html of the field
     */
    public String getFixForEditHtml()
    {
        final OperationContextImpl operationContext = new OperationContextImpl(IssueOperations.EDIT_ISSUE_OPERATION, getFieldValuesHolder());
        return getFixVersionField().getEditHtml(null, operationContext, this, getIssueObject(), getDisplayParams());
    }

    private Map<String, Object> getDisplayParams()
    {
        // This will render the field in it's "aui" state.
        final Map<String, Object> displayParams = new HashMap<String, Object>();
        displayParams.put("theme", "aui");
        return displayParams;
    }

    private Map getFieldValuesHolder() {
        if (fieldValuesHolder == null)
        {
            fieldValuesHolder = new HashMap();
        }
        return fieldValuesHolder;
    }

    private void includeResources() {
        pageBuilderService.assembler().resources().requireWebResource("jira.webresources:jira-fields");

    }

    private OrderableField getFixVersionField() {
        return ((OrderableField)getField("fixVersions"));
    }


    /**
     * Used by the decorator
     */
    public GenericValue getProject()
    {
        return getIssue().getProject();
    }

    /**
     * Used by the decorator
     */
    public Issue getIssue()
    {
        return getIssueObject();
    }

    public Issue getIssueObject()
    {
        final IssueService.IssueResult issueResult = issueService.getIssue(authenticationContext.getLoggedInUser(), id);
        if (!issueResult.isValid())
        {
            this.addErrorCollection(issueResult.getErrorCollection());
            return null;
        }

        return  issueResult.getIssue();
    }

    // Getter adn Setters for passing the form params

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long[] getFixVersions() {
        return fixVersions;
    }

    public void setFixVersions(Long[] fixVersions) {
        this.fixVersions = fixVersions;
    }
}
