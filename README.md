# Jira plugin to create merge issues

A simple JIRA plugin that adds the "Create merge issue(s)" action to issues to create new merge issue for a list of fix versions.

# Using the Atlassian SDK
This project contains a project created with the atlassian SDK.
Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-mvn package -- rebuilds and reloads the plugin in the current instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
